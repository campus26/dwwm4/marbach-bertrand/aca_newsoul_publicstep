<?php

function keepURL($urlVisit)
{
    session_start();
    $userID = $_SESSION['userID'] ?? null;
    $timeStartActualSession = $_SESSION['timeStartActualSession'] ?? null;
    $userNameFirst = $_SESSION['userNameFirst'] ?? null;
    $userNameLast = $_SESSION['userNameLast'] ?? null;
    session_write_close();

    $userIP = $_SERVER['REMOTE_ADDR'];
    if ($userID == null) {
        $userID = 6;
    }
    $timeStartUrlVisit = date('Y-m-d H:i:s');

    $servername = "51.91.150.5";
    $port = 9001;
    $username = "cm42qs4xr0q0m9kqrezj03728";
    $password = "rfWHtImfNgC14i3G5bcwdAE8";
    $dbname = "aca";

    $conn = new mysqli($servername, $username, $password, $dbname, $port);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $tableExists = $conn->query("SHOW TABLES LIKE 'sessions'")->num_rows > 0;
    if (!$tableExists) {
        $sql = "CREATE TABLE sessions (
            id INT(9) AUTO_INCREMENT PRIMARY KEY,
            user_id VARCHAR(3) NOT NULL,
            user_ip VARCHAR(45) NOT NULL,
            user_name_first VARCHAR(128) NOT NULL,
            user_name_last VARCHAR(128) NOT NULL,
            time_start_actual_session DATETIME NOT NULL,
            time_start_url_visit DATETIME NOT NULL,
            url_visit VARCHAR(256) NOT NULL
        )";
        if (!$conn->query($sql)) {
            die("Error creating table: " . $conn->error);
        }
    }

    $sql = "INSERT INTO sessions (user_id, user_ip, user_name_first, user_name_last, time_start_actual_session, time_start_url_visit, url_visit) VALUES (?, ?, ?, ?, ?, ?, ?)";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("sssssss", $userID, $userIP, $userNameFirst, $userNameLast, $timeStartActualSession, $timeStartUrlVisit, $urlVisit);

    if (!$stmt->execute()) {
        die("Error inserting record: " . $stmt->error);
    }

    // echo "<p style='background-color: green; color: white; margin: 0;'>New record inserted successfully</p>";

    $stmt->close();
    $conn->close();
}

?>