<?php
include("init/config-pages.php");
include("../assets/functions/functionsDB.php");

$title_page = $entity . " - DR 172L (F-BTUL)";

$breadcrumbs = array(
    array("url" => "../pages/index-landing-page.php", "title" => "Accueil"),
    array("url" => "../pages/flotte.php", "title" => "Flotte"),
);

keepURL($_SERVER['REQUEST_URI']);
include("templates/header.php");
include("sections/fleet_f172_page.php");
include("templates/footer.php");
include("templates/include_js_scripts.php");
?> 