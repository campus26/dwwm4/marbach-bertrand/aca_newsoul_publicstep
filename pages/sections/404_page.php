<div class="section">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="section-heading heading-center section-heading-animate">
                                <div class="section-subheading">Page non trouvée, désolé</div>
                                <h1>404</h1>
                                <p class="section-desc">Il y a eu erreur de frappe dans l'adresse ou la page recherchée
                                    a été déplacée (Error 404 : not found)</p>
                            </div>
                            <div class="btn-group align-items-center justify-content-center">
                                <a href="<?php echo __DIR__ . '/index.php'?>" class="btn btn-border">
                                    <span>Retour à la page d'accueil</span>
                                    <svg class="btn-icon-right" viewBox="0 0 13 9" width="13" height="9">
                                        <use xlink:href="../assets/img/sprite.svg#arrow-right"></use>
                                    </svg>
                                </a>
                                <a href="<?php echo __DIR__ ?>/contacts.php" class="btn btn-border">
                                    <span>Laissez-nous un message</span>
                                    <svg class="btn-icon-right" viewBox="0 0 13 9" width="13" height="9">
                                        <use xlink:href="../assets/img/sprite.svg#arrow-right"></use>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>