<div class="section">
				<div class="container">
					<div class="row">
						<header class="col-12">
							<div class="section-heading heading-center">
								<div class="section-subheading">70 années de vol</div>
								<h1>Quelques lignes a propos de nous</h1>
								<p class="section-desc">Notre passion et notre engagement pour l'aérien.</p>
							</div>
						</header>
						<div class="col-12">
							<div class="content">
								<div class="item-bordered item-border-radius">
									<img data-src="../assets/img/ACA2403F-BV-en-vol-Grande.jpg" class="img-cover img-responsive lazy"
										src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
										alt="">
								</div>
								<h2>Le mot du président</h2>
								<br>
								<p>C'est avec un grand paisir que je rédige aujourd'hui cet éditorial pour célébrer 
									en 2024 les 70 ans de notre aéro-club. Cette occasion spéciale marque non seulement les 
									nombreuses années de passion et de dévouement que nous avons dédiées à l'aviation, 
									mais aussi les nombreux souvenirs et les amitiés solides qui ont été nouées au fil 
									du temps.</p>
								<p>Depuis sa fondation en 1954, notre club a connu de nombreux changements et a su 
									s'adapter pour suivre les nombreuses évolutions du monde de l'aéronautique.
									Nous sommes heureux de compter cinq avions dans notre flotte, qui nous permettent 
									de voler en toute sécurité et d'explorer les merveilles de l'aviation ensemble.
									Ces avions sont le symbole de notre engouement pour l'aviation et de notre 
									détermination à poursuivre notre passion.</p>
								<p>Je tiens également à souligner l'importance de la formation des jeunes par le BIA 
									(Brevet d'Initiation Aéronautique). Nous pensons qu'il est essentiel de soutenir 
									les jeunes pilotes et de les encourager à poursuivre leur passion pour l'aviation.</p>
								<p>Depuis de nombreuses années l'Aéro-club de l'Ardèche, contribue à l'animation 
									touristique de notre région en proposant le survol des Gorges de l'Ardèche et de 
									notre magnifique région. Alors n'hésitez plus, et venez nous rencontrer pour un vol 
									découverte.</p>
								<p>Il faut aussi remercier tout particulièrement chacun des bénévoles pour leur 
									engagement envers le club. Je remercie également nos instructeurs talentueux pour 
									leur dévouement à enseigner aux nouveaux pilotes et à maintenir un environnement de 
									formation sûr et stimulant.
									Enfin, je remercie nos administratifs pour tout le travail de l'ombre pour assurer 
									le bon fonctionnement de notre club associatif.</p>
								<p>Pour terminer, je tiens à exprimer ma gratitude envers tous les annonceurs qui ont 
									répondu nombreux à notre demande, facilitant ainsi la parution de la plaquette dont  
									vous lisez ici quelques textes et que vous trouverez à l'aéro-club quand vous
									viendrez nous rencontrer à l'occasion, comme proposé dans ces lignes, d'un bien 
									beau vol de découverte.</p>
								<p>Bonne découverte de l'ensemble de nos activités.</p>
								<br>
								<p>Julien Meister, juillet 2023</p>
							</div>
						</div>
					</div>
				</div>
			</div>