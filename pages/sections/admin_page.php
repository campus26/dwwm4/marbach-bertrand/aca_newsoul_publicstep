<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-heading heading-center section-heading-animate">
                    <div class="section-subheading">Du côté de l'administration du site</div>
                    <h1>Administration du site</h1>
                    <p class="section-desc">c'est ici que tout se met en place</p>
                </div>

                <div class="col-12">
                    <?php

                    // here handling of sessions

                    echo "<br><h3> liste des accréditations par utilsateur</h3>";

                    $servername = "51.91.150.5";
                    $port = 9001;
                    $username = "cm42qs4xr0q0m9kqrezj03728";
                    $password = "rfWHtImfNgC14i3G5bcwdAE8";
                    $dbname = "aca";

                    // Creating connection
                    $conn = new mysqli($servername, $username, $password, $dbname, $port);

                    // Checking connection
                    if ($conn->connect_error) {
                        die("<p style='background-color: red; color: white; margin: 0;'>&nbsp;&nbsp;⇥ Connection failed: " . $conn->connect_error . "</p>");
                    }
                    echo "<p style='background-color: blue; color: white; margin: 0;'>&nbsp;&nbsp;⇥ Connected successfully</p>";

                    // Check if the table exists
                    $tableExists = $conn->query("SHOW TABLES LIKE 'users'")->num_rows > 0;

                    if (!$tableExists) {
                        // Create table only if it doesn't exist
                        $sql = "CREATE TABLE users(
                            id INT(3) PRIMARY KEY AUTO_INCREMENT,
                            entity_user_id VARCHAR(7) NOT NULL,
                            date_insert DATETIME NOT NULL,
                            date_last_update DATETIME NOT NULL,
                            entity VARCHAR(128) NOT NULL,
                            name_first VARCHAR(128) NOT NULL,
                            name_last VARCHAR(128) NOT NULL,
                            email VARCHAR(128) NOT NULL,
                            password VARCHAR(256) NOT NULL,
                            phone_mobile_number VARCHAR(50) NOT NULL,
                            grinch_level VARCHAR(1) NOT NULL,
                            session_time_out INT(4) NOT NULL,
                            time_start_actual_session DATETIME NOT NULL,
                            has_acknowledged_terms_conditions TINYINT(1) NOT NULL,
                            profile_picture_url VARCHAR(256) NOT NULL,
                            ext_storage_path VARCHAR(256) NOT NULL
                        )";

                        if ($conn->query($sql) === TRUE) {
                            echo "<p style='background-color: blue; color: white; margin: 0;'>&nbsp;&nbsp;⇥ Table 'users' created successfully <br></p>";
                        } else {
                            echo "<p style='background-color: red; color: white; margin: 0;'>&nbsp;&nbsp;⇥⇥⇥ Error creating table : " . $conn->error . "<br></p>";
                        }
                    } else {
                        echo "<p style='background-color: black; color: white; margin: 0;'>&nbsp;&nbsp;⇥⇥ Table 'users' exists <br></p>";
                    }

                    // $password = hash('sha256', 'ViveLeDev!');
                    // // ViveLeDev! EscargotMusicalPlanant$ PélicanPaisible&Polarisé Crevett3VoltigeanteEnjouée EscargotMusicalPlanant$ h3liceJuste&Parfaite
                    // $sql = "INSERT INTO users (id, entity_user_id, date_insert,date_last_update, entity, name_first, name_last, email, password, phone_mobile_number, grinch_level, session_time_out, time_start_actual_session, has_acknowledged_terms_conditions, profile_picture_url, ext_storage_path)
                    //     VALUES (
                    //     0, 'MBH', now(),now(), 'ACA', 'Bertrand', 'MARBACH', 'bertrand.marbach@oumpahpah.fr', '$password', '+33.6.519.777.46', 'G', 0, NOW(), 1, '../assets/img/icons/statusIsConnected.png', 'https://storage.marbach-bertrand.ovh/'
                    //     )
                    //     1, '', now(), now(), 'ACA', 'Jaqueline', 'COCHRAN', 'bureau@autremail.aero', '$password', '+33.6.80.84.72.97', 'R', 1800, '', 1, '../assets/img/icons/statusIsConnected.png', 'https://storage.marbach-bertrand.ovh/'
                    //     )
                    //     2, '', now(), now(), 'ACA', 'Hélène', 'BOUCHER', 'organisme-formation-pn.dsac-ce@aviation-civile.gouv.fr', '$password', '+33.6.80.84.72.97', 'I', 1800, '', 1, '../assets/img/icons/statusIsConnected.png', 'https://storage.marbach-bertrand.ovh/'
                    //     )
                    //     3, '', now(), now(), 'ACA', 'Bob', 'HOOVER', 'comiteLFHF@autremail.aero', '$password', '+33.6.80.84.72.97', 'N', 1800, '', 1, '../assets/img/icons/statusIsConnected.png', 'https://storage.marbach-bertrand.ovh/'
                    //     )
                    //     4, '', now(), now(), 'ACA', 'Bessie', 'COLEMAN', 'contact@aeroclubdelardeche.com', '$password', '+33.6.80.84.72.97', 'C', 1800, '', 1, '../assets/img/icons/statusIsConnected.png', 'https://storage.marbach-bertrand.ovh/'
                    //     )
                    //     5, '', now(), now(), 'ACA', 'Jacqueline', 'AURIOL', 'contact@autremail.tdp', '$password', '+33.6.80.84.72.97', 'H', 1800, '', 1, '../assets/img/icons/statusIsConnected.png', 'https://storage.marbach-bertrand.ovh/'
                    //     )

                    // ON DUPLICATE KEY UPDATE
                    //     entity_user_id = VALUES(entity_user_id),
                    //     date_last_update = VALUES(date_last_update), 
                    //     entity = VALUES(entity),
                    //     name_first = VALUES(name_first),
                    //     name_last = VALUES(name_last),
                    //     email = VALUES(email),
                    //     password = VALUES(password), 
                    //     phone_mobile_number = VALUES(phone_mobile_number),
                    //     grinch_level = VALUES(grinch_level),
                    //     session_time_out = VALUES(session_time_out),
                    //     time_start_actual_session = VALUES(time_start_actual_session,
                    //     has_acknowledged_terms_conditions = VALUES(has_acknowledged_terms_conditions),
                    //     profile_picture_url = VALUES(profile_picture_url),
                    //     ext_storage_path = VALUES(ext_storage_path)";

                    // if ($conn->query($sql) === TRUE) {
                    //     if ($conn->affected_rows > 0) {
                    //         echo "<p style='background-color: blue; color: white; margin: 0;'>&nbsp;&nbsp;⇥ Record inserted/updated successfully.</p>";
                    //         echo "<p style='background-color: blue; color: white; margin: 0;'>&nbsp;&nbsp;⇥ Number of fields updated: " . $conn->affected_rows. "</p>";
                    //     } else {
                    //         echo "<p style='background-color: black; color: white; margin: 0;'>&nbsp;&nbsp;⇥⇥ Record already exists and no fields were updated. <br></p>";
                    //     }
                    // } else {
                    //     echo "<p style='background-color: red; color: white; margin: 0;'>&nbsp;&nbsp;⇥⇥⇥ ERROR: Could not able to execute $sql. " . $conn->error . "<br></p>";
                    // }

                    $sql = "SELECT id, entity_user_id, grinch_level, 
                        DATE_FORMAT(date_insert, '%d/%m/%y %H:%i:%s') AS formatted_date_insert,
                        DATE_FORMAT(date_last_update, '%d/%m/%y %H:%i:%s') AS formatted_date_last_update,
                        entity, name_first, name_last, email, phone_mobile_number, 
                        session_time_out, DATE_FORMAT(time_start_actual_session, '%d/%m/%y %H:%i:%s') AS formatted_time_start_actual_session,
                        has_acknowledged_terms_conditions, profile_picture_url, ext_storage_path 
                        FROM users 
                        ORDER BY id ASC, name_last ASC";
                    $result = $conn->query($sql);

                    echo "<div style='overflow-x: auto;'>";
                    echo "<table style='margin: 0; padding: 0; font-size: 0.8rem;'>";
                    echo "<tr style='margin: 0; padding: 0;background-color: lightcyan; color: black;'>";
                    echo "<th style='padding: 5px;'>ID</th>";
                    echo "<th style='padding: 5px;'>User ID</th>";
                    echo "<th style='padding: 5px;'>Grinch Level</th>";
                    echo "<th style='padding: 5px;'>Insert</th>";
                    echo "<th style='padding: 5px;'>Update</th>";
                    echo "<th style='padding: 5px;'>Actual Session</th>";
                    echo "<th style='padding: 5px;'>Entity</th>";
                    echo "<th style='padding: 5px;'>First Name</th>";
                    echo "<th style='padding: 5px;'>Last Name</th>";
                    echo "<th style='padding: 5px;'>Email</th>";
                    echo "<th style='padding: 5px;'>Mobile</th>";
                    echo "<th style='padding: 5px;'>Time Out</th>";
                    echo "<th style='padding: 5px;'>TnC OK </th>";
                    echo "<th style='padding: 5px;'>Profile Picture URL</th>";
                    echo "<th style='padding: 5px;'>Ext Storage Path</th>";
                    echo "</tr>";

                    $row_count = 0;
                    while ($row = $result->fetch_assoc()) {
                        $row_count++;
                        $row_color = $row_count % 2 == 0 ? 'lightgrey' : 'white';


                        if (isset($row['time_start_actual_session'])) {
                            $timeNow = strtotime(date("Y-m-d H:i:s"));
                            $sessionStart = strtotime($row['time_start_actual_session']);
                            $sessionTimeout = $row['session_time_out'] * 60;

                            if ($row['session_time_out'] == 0 || ($timeNow - $sessionStart) <= $sessionTimeout) {
                                $sessionColor = 'background-color: green; color: white;';
                            } else {
                                $sessionColor = '';
                            }
                        } else {
                            $sessionColor = '';
                        }

                        echo "<tr style='background-color: $row_color; color: blue;'>";
                        echo "<td style='padding: 5px; text-align: right;'>{$row['id']}</td>";
                        echo "<td style='padding: 5px;'>{$row['entity_user_id']}</td>";
                        echo "<td style='padding: 5px; text-align: center;'>{$row['grinch_level']}</td>";
                        echo "<td style='padding: 5px;'>{$row['formatted_date_insert']}</td>";
                        echo "<td style='padding: 5px;'>{$row['formatted_date_last_update']}</td>";
                        echo "<td style='padding: 5px; $sessionColor'>{$row['formatted_time_start_actual_session']}</td>";
                        echo "<td style='padding: 5px;'>{$row['entity']}</td>";
                        echo "<td style='padding: 5px;'>{$row['name_first']}</td>";
                        echo "<td style='padding: 5px;'>{$row['name_last']}</td>";
                        echo "<td style='padding: 5px;'>{$row['email']}</td>";
                        echo "<td style='padding: 5px;'>{$row['phone_mobile_number']}</td>";
                        echo "<td style='padding: 5px;text-align: center;'>{$row['session_time_out']}</td>";
                        echo "<td style='padding: 5px;'>{$row['has_acknowledged_terms_conditions']}</td>";
                        echo "<td style='padding: 5px;'>{$row['profile_picture_url']}</td>";
                        echo "<td style='padding: 5px;'>{$row['ext_storage_path']}</td>";
                        echo "</tr>";
                    }
                    echo "</table>";

                    echo "</div>";

                    echo "<p style='background-color: blue; color: white; margin: 0;'>&nbsp;&nbsp;⇥ Total entries displayed: $row_count, ranked by privilege • time in server GMT</p>";

                    // $conn->close();

                    // here handling of sessions
                    echo "<br><br><h3>Historique des sessions et des navigations</h3>";

                    // $servername = "51.91.150.5";
                    // $port = 9000;
                    // $username = "clutnfl1t000b9kqr83z15hnf";
                    // $password = "lIJz2YhIgSbgzmaZs0Nf2BPx";
                    // $dbname = "aca";

                    $conn = new mysqli($servername, $username, $password, $dbname, $port);

                    if ($conn->connect_error) {
                        die("<p style='background-color: red; color: white; margin: 0;'>⇥ Connection failed: " . $conn->connect_error . "</p>");
                    }

                    $tableExists = $conn->query("SHOW TABLES LIKE 'sessions'")->num_rows > 0;
                    if (!$tableExists) {
                        $sql = "CREATE TABLE sessions (
                            id INT(9) AUTO_INCREMENT PRIMARY KEY,
                            user_id VARCHAR(3) NOT NULL,
                            user_ip VARCHAR(45) NOT NULL,
                            user_name_first VARCHAR(128) NOT NULL,
                            user_name_last VARCHAR(128) NOT NULL,
                            time_start_actual_session DATETIME NOT NULL,
                            time_start_url_visit DATETIME NOT NULL,
                            url_visit VARCHAR(256) NOT NULL
                        )";

                        if ($conn->query($sql) === FALSE) {
                            die("<p style='background-color: red; color: white; margin: 0;'>⇥ Error creating table : " . $conn->error . "</p>");
                        }
                    }

                    $sql = "SELECT id, user_id, user_ip, user_name_first, user_name_last, 
                            DATE_FORMAT(time_start_actual_session, '%d/%m/%y %H:%i:%s') AS formatted_time_start_actual_session, 
                            DATE_FORMAT(time_start_url_visit, '%d/%m/%y %H:%i:%s') AS formatted_time_start_url_visit, 
                            url_visit FROM sessions 
                            ORDER BY id DESC, user_name_last ASC";
                    $result = $conn->query($sql);

                    echo "<div style='overflow-x: auto; overflow-y: scroll; max-height: 400px; '>";
                    echo "<table style='margin: 0; padding: 0; font-size: 0.8rem;'>";
                    echo "<thead>";
                    echo "<tr style='margin: 0; padding: 0;background-color: lightcyan; color: black;'>";
                    echo "<th style='padding: 5px;'>ID</th>";
                    echo "<th style='padding: 5px;'>User ID</th>";
                    echo "<th style='padding: 5px;'>User IP</th>";
                    echo "<th style='padding: 5px;'>First Name</th>";
                    echo "<th style='padding: 5px;'>Last Name</th>";
                    echo "<th style='padding: 5px;'>Session</th>";
                    echo "<th style='padding: 5px;'>When</th>";
                    echo "<th style='padding: 5px;'>URL</th>";
                    echo "</tr>";
                    echo "</thead>";
                    echo "<tbody>";

                    $row_count = 0;
                    while ($row = $result->fetch_assoc()) {
                        $row_count++;
                        $row_color = $row_count % 2 == 0 ? 'lightgrey' : 'white';

                        echo "<tr style='background-color: $row_color; color: blue;'>";
                        echo "<td style='padding: 5px; text-align: right;'>{$row['id']}</td>";
                        echo "<td style='padding: 5px; text-align: right;'>{$row['user_id']}</td>";
                        echo "<td style='padding: 5px;'>{$row['user_ip']}</td>";
                        echo "<td style='padding: 5px;'>{$row['user_name_first']}</td>";
                        echo "<td style='padding: 5px;'>{$row['user_name_last']}</td>";
                        echo "<td style='padding: 5px;'>{$row['formatted_time_start_actual_session']}</td>";
                        echo "<td style='padding: 5px;'>{$row['formatted_time_start_url_visit']}</td>";
                        echo "<td style='padding: 5px;'>{$row['url_visit']}</td>";
                        echo "</tr>";
                    }
                    echo "</tbody>";
                    echo "</table>";
                    
                    echo "</div>";
                    echo "<p style='background-color: blue; color: white; margin: 0;'>&nbsp;&nbsp;⇥ Total entries displayed: $row_count • Time in server GMT</p>";


                    $result->close();
                    $conn->close();

                    ?>

                </div>
                <div style='margin : 0; padding : 0;'> <br> </div>
                <div class="btn-group align-items-center justify-content-center">

                    <a href="../index-landing-page.php" class="btn btn-border">
                        <span>Retour à la page d'accueil</span>
                        <svg class="btn-icon-right" viewBox="0 0 13 9" width="13" height="9">
                            <use xlink:href="../assets/img/sprite.svg#arrow-right"></use>
                        </svg>
                    </a>
                    <a href="../pages/contacts.php" class="btn btn-border">
                        <span>Formulaire de contact</span>
                        <svg class="btn-icon-right" viewBox="0 0 13 9" width="13" height="9">
                            <use xlink:href="../assets/img/sprite.svg#arrow-right"></use>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>