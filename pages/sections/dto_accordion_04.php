<li class="accordion-item section-bgc">
									<div class="accordion-trigger">
										<div><a href="#!" target="_blank" title="" style="display: inline-block;">
												<img src="../assets/img/ACA2403-Icone-apprentissage-en-ligne.png"
													alt="icone d'incident et d'alerte'" width="44" height="44">
											</a>∾ Formations déclarées
										</div>
									</div>
									<div class="accordion">
										<div class="row gutters-default">
											<div class="accordion-content content">
												<table class="table-secondary">
													<thead>
														<tr>
															<th>Lien</th>
															<th>N°</th>
															<th>Date</th>
															<th>Titre</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-Cepadues_190100-table-des-matieres.pdf"
																target="_blank">
																<span><i
																		class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Cepadues<br>
																		<a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO•Aerogligli-Manuel_de_formation_theorique_e-learning_DTO v4.pdf"
																		target="_blank">
																		<span><i
																				class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Aérogligli</td>
															<td>v?<br>v4</td>
															<td>Sept 2022<br>04/12/23</td>
															<td>Manuel du Pilote <br>MANUEL DE FORMATION THEORIQUE LAPL (A) / PPL(A) E-learning DTO</td>
														</tr>
														<tr>
															<td><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-LAPL-Cepadues-190100_poster des Matrices.pdf"
																target="_blank">
																<span><i
																		class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Cepadues</td>
															<td>v19</td>
															<td>Sept 2022</td>
															<td>Cepadues, Pratique LAPL</td>
														</tr>
														<tr>
															<td><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-ANPI-Livret-formation-LAPLA-vers-PPLA-DTO-REV-1-Version-1-01-Fevrier-2023.pdf"
																target="_blank">
																<span><i
																		class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ ANPI
																		<?php if ($grinchLevel <= 1031) { ?><br>
																		<a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-SEP-T-Standardisation-Rev-1-V-1-MBH-230423.pdf"
																		target="_blank">
																		<span><i
																				class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Std MBH<br>
																				<a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-SEP-T-Standardisation-Rev-1-V-1-VL-230422.pdf"
																				target="_blank">
																				<span><i
																						class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Std VL<?php } ?></td>
															<td>v1</td>
															<td>01/02/23</td>
															<td>ANPI-LIVRET FORMATION LAPL(A) VERS PPL(A) REV 1 – VERSION 1</td>
														</tr>
														<tr>
															<td><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-LAPL-Cepadues-190100_poster des Matrices.pdf"
																target="_blank">
																<span><i
																		class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Cepadues</td>
																		<td>v19</td>
																		<td>Sept 2022</td>
																		<td>Cepadues, Pratique PPL</td>
														</tr>
														</tr>
														<tr>
															<td><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2304-DTO-ANPI•DTO•SEPT•Livret-stagiaire-SEPT-REV-1-V1-20-Fevrier-2023.pdf"
																target="_blank">
																<span><i
																		class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ ANPI</td>
																		<td>v1</td>
																		<td>20/02/23</td>
																		<td>FORMATION SEP(LAND)</td>
														</tr>
														<tr>
															<td><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-ANPI-VDN-Livret-formation-V8.pdf"
																target="_blank">
																<span><i
																		class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ ANPI</td>
															<td>v1</td>
															<td>08/04/22</td>
															<td>ANPI-FORMATION VDN</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</li>