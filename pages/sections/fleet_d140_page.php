<div class="section">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="news-post">
                                <header class="news-post-header">
                                    <h1 class="news-post-title">DR140 C N°128 F-GEBV</h1>
                                    <div class="news-post-meta">
                                        <div class="news-post-meta-item">
                                            <i class="material-icons md-22">access_time</i>
                                            <span>2 avril 1973</span>
                                        </div>
                                        <div class="news-post-meta-item">
                                            <span>conception, construction : &nbsp;</span>
                                            <a href="#!">Delemontez, S.A.N. (Bernay)</a>
                                        </div>
                                        <div class="news-post-meta-item">
                                            <i class="material-icons md-20">chat_bubble</i>
                                            <span>18 (mais on peut aussi écrire une petite accroche)</span>
                                        </div>
                                    </div>
                                    <div class="news-post-img item-bordered item-border-radius">
                                        <img data-src="../assets/img/ACA2403-FGEBV.jpg" class="img-responsive lazy"
                                            src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
                                            alt="">
                                    </div>
                                    <br>
                                </header>
                                <article class="news-post-article content">
                                   
                                    <h2>Documents et paramètres à considérer</h2>
        
                                    <blockquote>Ci-dessous les principales caractéristiques de l'aéronef et plus bas
                                        un tableau des principaux paramètres (liste non exhaustive) que le pilote 
                                        utilisera pour préparer et planifier son vol
                                    </blockquote>

                                    <ul>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FGEBV-ManuelVol.pdf" download="F-BV Manuel de Vol.pdf" target="_blank">Manuel de Vol</a></li>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA1903-FGEBV-fiche-pesee.pdf" download="F-BV Fiche de pesée.pdf" target="_blank">Fiche de pesée</a></li>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FGEBV-CL.pdf" download="F-BV Check List.pdf" target="_blank">Check-List</a></li>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FGEBV-DossierVol.pdf" download="F-BV Dossier de vol.pdf" target="_blank">Dossier de Vol</a></li>
                                        <!-- <li><a href="<php echo $extStoragePath; ?>aca-pdf/ACA2403-FGEBV-Phraseologie-de-base-v1.31.pdf" download="F-CH Phraséologie de base.pdf" target="_blank">Phraséologie de base</a></li> -->
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FGEBV-remplir-carnet-de-route.pdf" download="F-BV Memo carnet de route.pdf" target="_blank">Mémo carnet de route</a></li>
                                    </ul>
                                    <br>
                                    <table style="text-align:center">
                                        <thead>
                                            <tr>
                                                <th>Moteur</th>
                                                <th>Hélice</th>
                                                <th>Vx</th>
                                                <th>Vy</th>
                                                <th>Vi (75%)</th>
                                                <th>Carburant
                                                    total
                                                </th>
                                                <th>Carburant
                                                    inutil.</th>
                                                <th>Conso 75%
                                                    (2500t/min)
                                                </th>
                                                <th>Conso
                                                    (local)</th>
                                                <th>Conso
                                                    (TdP)
                                                </th>
                                                <th>Chge utile</th>
                                                <th>MTOW</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td >Lycoming<br>
                                                    O 360 A<br>
                                                    180CV
                                                </td>
                                                <td >Mac Cauley<br>
                                                    1A 200 FA<br>
                                                    8056
                                                </td>
                                                <td>125</td>
                                                <td>150</td>
                                                <td>220</td>
                                                <td>AV 90<br>
                                                    sup 45<br>
                                                    AR 125
                                                </td>
                                                <td>AV 0<br>
                                                    AR 7 (?)</td>
                                                <td>40</td>
                                                <td>35</td>
                                                <td>30</td>
                                                <td>524</td>
                                                <td>1200</td>
                                            </tr>   
                                            <tr style="font-size: 0.8em; font-style: italic">
                                                <td style="font-size: 1em; text-align: right">unité</td>
                                                <td></td>
                                                <td >km/h</td>
                                                <td>km/h</td>
                                                <td>km/h</td>
                                                <td>L</td>
                                                <td>L</td>
                                                <td>L/h</td>
                                                <td>L/h</td>
                                                <td>L/h</td>
                                                <td>kg</td>
                                                <td>kg</td>
                                            </tr>                                        
                                        </tbody>
                                        <tfoot>
                                            <!-- <tr>
                                                <td>unité</td>
                                                <td></td>
                                                <td>Km/h</td>
                                                <td>Km/h</td>
                                                <td>Km/h</td>
                                                <td>Lt</td>
                                                <td>Lt</td>
                                                <td>Lt/h</td>
                                                <td>Lt/h</td>
                                                <td>Lt/h</td>
                                                <td>kg</td>
                                                <td>kg</td>
                                            </tr> -->
                                        </tfoot>
                                    </table>
                                    <p>Bonne préparation !</p>
                                    <p>L'équipe pédagogique et le mécano sont là pour vous aider à lever les doutes</p>
                                </article>
                                <footer class="news-post-footer">
                                    <div class="row align-items-center justify-content-between items">
                                        <div class="col-md col-12 item">
                                            <ul class="news-post-cat">
                                                <li><a href="../assets/img/ACA2403-FGEBV-DB.jpg" target="_blank">Tableau de bord</a></li>
                                                <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FGEBV-ManuelVol-clair.pdf" download="F-BV manuel de vol clair.pdf" target="_blank">Manuel de vol (clair)</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </footer>
                            </div>
                        </div>
                    </div>
                </div>
            </div>