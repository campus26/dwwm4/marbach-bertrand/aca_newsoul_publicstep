<div class="section">
                <div class="container">
                    <div class="row items">
                        <div class="col-12">
                            <div class="content">
                                <!-- <div class="item-bordered item-border-radius">
									<img data-src="assets/img/ACA2403F-BV-en-vol-Grande.jpg" class="img-cover img-responsive lazy"
										src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
										alt="">
								</div> -->
                                <h2>Bienvenue dans la page de l'École de pilotage</h2>

                                <p>Sont regroupées ici des ressources délarées auprès de la DGAC.<br><br>
                                    Mais pas uniquement !<br><br>

                                    En complément de celles-ci, il y a d'autres supports utilisés. Chacun(e),
                                    élève pilote ou pilote au bénéfice de sa licence trouvera, on l'espère, de quoi
                                    préparer ses séances de vol, sa progression théorique, rafaîchir ses compétences et
                                    connaissances.<br><br>

                                    Bons atterrissages !<br></p>
                            </div>
                        </div>

                        <header class="col-12">
                            <div class="section-heading heading-center">
                                <div class="section-subheading">elles sont techniques et non-techniques</div>
                                <h1>Les compétences</h1>
                            </div>
                        </header>
                        <div class="col-lg-4 col-md-6 col-12 item" style="overflow: hidden;">
                            <!-- Begin left news item -->
                            <article class="news-item item-style"
                                style="display: flex; justify-content: center; align-items: center;">
                                <a href="#!" class="news-item-img el" style="flex: 1; background-color: white;">
                                    <img data-src="../assets/img/ACA2403-CompetencesFI.jpeg" class="lazy"
                                        src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
                                        alt="" style="max-width: 95%; max-height: 95%; object-fit: contain;">
                                </a>
                            </article>
                        </div>


                        <div class="col-lg-4 col-md-6 col-12 item" style="overflow: hidden;">
                            <!-- Begin middle news item -->
                            <article class="news-item item-style" style="display: flex; align-items: center;">
                                <a href="https://<?php echo $extStoragePath; ?>aca-video/ACA2304-Integration-Tour-de-Piste.m4v"
                                    class="news-item-img el" style="flex: 1;" onclick="playVideo(event)">
                                    <!-- If you want to show an image placeholder, add an <img> tag here -->
                                    <!-- For example: <img src="placeholder.jpg" alt="Video Placeholder"> -->
                                </a>
                                <video class="lazy" controls autoplay
                                    style="max-width: 100%; height: auto; object-fit: cover;">
                                    <source
                                        src="https://storage.marbach-bertrand.ovh/aca-video/ACA2304-Integration-Tour-de-Piste.m4v"
                                        type="video/mp4">
                                    Ce navigateur internet ne permet pas de lire les vidéos.
                                </video>
                            </article>
                            <!-- End middle news item -->
                        </div>

                        <div class="col-lg-4 col-md-6 col-12 item" style="overflow: hidden;">
                            <!-- Begin rightnews item -->
                            <article class="news-item item-style"
                                style="display: flex; justify-content: center; align-items: center;">
                                <a href="#!" class="news-item-img el" style="flex: 1; background-color: white;">
                                    <img data-src="../assets/img/ACA2403-CompetencesPilote.jpeg" class="lazy"
                                        src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
                                        alt="" style="max-width: 95%; max-height: 95%; object-fit: contain;">
                                </a>
                                <!-- End right news item -->
                        </div>

                        <div class="col-lg-4 col-md-6 col-12 item" style="overflow: hidden;">
                            <!-- Begin left news item -->
                            <article class="news-item item-style">
                                <div class="news-item-info" style="text-align: center;">
                                    <h2 class="news-item-heading item-heading">
                                        <a href="#!" title="">Instructeur</a>
                                    </h2>
                                </div>
                            </article>
                        </div>

                        <div class="col-lg-4 col-md-6 col-12 item" style="overflow: hidden;">
                            <!-- Begin middle news item -->
                            <article class="news-item item-style">
                                <div class="news-item-info" style="text-align: center;">
                                    <h2 class="news-item-heading item-heading">
                                        <a href="#!" title=""><i class="material-icons md-18">chevron_left</i><i
                                                class="material-icons md-18">chevron_right</i></a>
                                    </h2>
                                </div>
                            </article>
                            <!-- End middle news item -->
                        </div>

                        <div class="col-lg-4 col-md-6 col-12 item" style="overflow: hidden;">
                            <!-- Begin right news item -->
                            <article class="news-item item-style">
                                <div class="news-item-info" style="text-align: center;">
                                    <h2 class="news-item-heading item-heading">
                                        <a href="#!" title="">Pilote</a>
                                    </h2>
                                </div>
                            </article>
                            <!-- End right news item -->
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <section class="section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section-heading heading-center">
                            <h2>Demandez les programmes !</h2>
                        </div>
                        <div class="col-12 item">
                            <p>Le DTO (organisme de formation déclaré, <strong>D</strong>eclared
                                <strong>T</strong>raining
                                <strong>O</strong>rganisation) est l'appellation
                                administrative
                                d'une école de pilotage dans l'Union Européenne. Une partie de nos programmes est
                                déposée auprès
                                de la DGAC. Une
                                autre ne l'est pas car elle n'entre pas dans le champ règlementaire du DTO, par exemple
                                les
                                lâchés
                                machines. Cela relève alors du règlement intérieur.
                                Mais pour notre école, la force et la rigueur seront les mêmes : nous nous devons d'être
                                garants
                                de la
                                sécurité des vols
                            </p>

                            <h5>Exposé des programmes</h5>
                            <ul>
                                <li>ACA : BIA (Brevet d'Initiation Aéronautique)</li>
                                <li>DTO : formation théorique LAPL et PPL</li>
                                <li>DTO : formation pratique LAPL</li>
                                <li>DTO : formation LAPL vers PPL</li>
                                <li>DTO : formation pratique PPL</li>
                                <li>DTO : qualification de type SEP (L) (renouvellement PPL)</li>
                                <li>DTO : qualification vol de nuit</li>
                                <li>ACA : lâché machine</li>
                                <li>ACA : Perfectionnement PPL</li>
                            </ul>
                        </div>
                        
                        <div class="tabs tabs-vertical">
                            <ul class="tabs-nav section-bgc">
                                <li class="active">BIA</li>
                                <li>Théorique LAPL et <strong>PPL</strong></li>
                                <li>Pratique LAPL</li>
                                <li>LAPL vers PPL</li>
                                <li><strong>Pratique PPL</strong></li>
                                <li>Qualification classe SEP (L)</li>
                                <li>Qualification vol de nuit</li>
                                <li>Lâché machine</li>
                                <li>ACA : Perfectionnement PPL</li>
                            </ul>
                            <div class="tabs-container">
                                                                
                                <?php
                                //  <!-- BIA -->
                                    include("flying_school_paragraph_01.php");

                                 //  <!-- Formation théorique LAPL et PPL -->    
                                    include("flying_school_paragraph_02.php");

                                //  <!-- Formation pratique LAPL -->  
                                    include("flying_school_paragraph_03.php");

                                //  <!-- Conversion LAPL vers PPL -->  
                                    include("flying_school_paragraph_04.php");

                                //  <!-- Formation Pratique PPL -->  
                                    include("flying_school_paragraph_05.php");

                                //  <!-- Qualification classe SEP (L) -->  
                                    include("flying_school_paragraph_06.php");

                                //  <!-- Qualification Vol de Nuit -->  
                                    include("flying_school_paragraph_07.php");

                                //  <!-- "Lâché" machine" -->  
                                    include("flying_school_paragraph_08.php");

                                //  <!-- Perfectionnement PPL -->     
                                    include("flying_school_paragraph_09.php");
                                ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="container">
                <div class="section-heading heading-center">
                    <div class="intro-content" style="--margin-left: 4rem;">
                        <div class="btn-group intro-btns">
		    				<div class="col">
                                S'il était convenable de vous proposer une alternative, la voici 😉 :
		    	                <a href="https://www.gorges-ardeche-pontdarc.fr/activite/les-gorges-de-lardeche-vues-du-ciel-labeaume/" target="_blank" class="btn btn-border btn-with-icon btn-small ripple">
	    			    	        <span>Vol découverte</span>
    					            <svg class="btn-icon-right" viewBox="0 0 13 9" width="13" height="9">
					    	            <use xlink:href="../assets/img/sprite.svg#arrow-right"></use>
				    	            </svg>
    			    	        </a>
                                ou encore, nous contacter pour...
		        		        <a href="../pages/contacts.php" data-title="connexion" class="btn btn-border btn-with-icon btn-small ripple">
	    	    		            <span>... une demande d'info</span>
    			    		        <svg class="btn-icon-right" viewBox="0 0 13 9" width="13" height="9">
					    	            <use xlink:href="../assets/img/sprite.svg#arrow-right"></use>
					                </svg>
				                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>