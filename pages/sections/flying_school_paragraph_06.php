                                <!-- Qualification classe SEP (L) -->
                                 <div class="tabs-item content">
                                    <p>Le support présenté ici servira pour permettre aux pilotes ayant laissé passer
                                        -probablement pour des raisons tès valables, ou simplement de la distraction
                                        (!)-
                                        la date d'échéance de leurs privilèges SEP. <br>
                                        Ce même programme sera utilisé quand un pilote ayant "raccroché les gants"
                                        souhaite reprende
                                        les vols.<br><br>

                                        <a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2304-DTO-ANPI•DTO•SEPT•Livret-stagiaire-SEPT-REV-1-V1-20-Fevrier-2023.pdf"
                                            target="_blank">
                                            <span><i
                                                    class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>
                                        ⇥ Le programme suivi est le programme de l'ANPI qui peut être téléchargé ici pour
                                        référence.
                                    </p>
                                </div>