<div class="section-bgc intro">
				<div class="intro-item intro-item-type-1" style="background-image: url('../assets/img/ACA2403imgFondSampzonAlisson.jpg');">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="intro-content" style="--margin-left: 4rem;">
									<div class="section-heading shm-none">
										<div class="section-subheading">ACA - Aéroclub de l'Ardèche</div>
										<h1>Une flotte de rêve dans un écrin entre 3 rivières</h1>
										<p class="section-desc">Trois avions à train classique, deux avions à train
											tricycle. Deux avions en bois et toile, les autres en métal.
											<br>Du vol local au voyage en passant par l'école, prenez l'air à bord
											de cockpits traditionnels ou modernisés !<br><br>

											Choississez votre avion pour votre vol de découverte (Baptême
											de l'air), venez apprendre, devenez pilote, venez voler avec nous...
										</p>
									</div>
									<div class="btn-group intro-btns">
										<a href="https://www.gorges-ardeche-pontdarc.fr/activite/les-gorges-de-lardeche-vues-du-ciel-labeaume/" target="_blank" class="btn btn-border btn-with-icon btn-small ripple">
											<span>Vol découverte</span>
											<svg class="btn-icon-right" viewBox="0 0 13 9" width="13" height="9">
												<use xlink:href="../assets/img/sprite.svg#arrow-right"></use>
											</svg>
										</a>
										<a href="../pages/index-onboarding.php" data-title="connexion" class="btn btn-border btn-with-icon btn-small ripple">
											<span>Accès adhérents</span>
											<svg class="btn-icon-right" viewBox="0 0 13 9" width="13" height="9">
												<use xlink:href="../assets/img/sprite.svg#arrow-right"></use>
											</svg>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>