<div class="section">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                                <h2>Comité de direction</h2>
                                <p>les adhérents de l'aéroclub élus au comité de direction sont :</p>
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Nom</th>
                                            <th>Prénom</th>
                                            <th>Qualité</th>
                                            <th>Élection</th>
                                            <th>N° tél</th>
                                            <th>Courriel</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>ABEILLON</td>
                                            <td>Paul</td>
                                            <td>Vice-Président</td>
                                            <td>2021</td>
                                            <td><span><i
                                                class="material-icons material-icons-outlined md-18">phone_locked</i></span></a></td>
                                            <!-- <td>paul.abeillon@orange.fr</td> -->
                                        </tr>
                                        <tr>
                                            <td>BARBOT</td>
                                            <td>Régis</td>
                                            <td>Membre</td>
                                            <td>2023</td>
                                            <td><span><i
                                                class="material-icons material-icons-outlined md-18">phone_locked</i></span></a></td>
                                            <!-- <td>barbotregis@gmail.com</td> -->
                                        </tr>
                                        <tr>
                                            <td>BERTRAND</td>
                                            <td>Pierre</td>
                                            <td>Trésorier</td>
                                            <td>2021</td>
                                            <td><span><i
                                                class="material-icons material-icons-outlined md-18">phone_locked</i></span></a></td>
                                            <!-- <td>bertrand.bernadette@wanadoo.fr</td> -->
                                        </tr>
                                        <tr>
                                            <td>CIVINS</td>
                                            <td>Claude</td>
                                            <td>Membre</td>
                                            <td>2022</td>
                                            <td><span><i
                                                class="material-icons material-icons-outlined md-18">phone_locked</i></span></a></td>
                                            <!-- <td>civins.claude@orange.fr</td> -->
                                        </tr>
                                        <tr>
                                            <td>DEDEUS</td>
                                            <td>Gilles</td>
                                            <td>Membre</td>
                                            <td>2022</td>
                                            <td><span><i
                                                class="material-icons material-icons-outlined md-18">phone_locked</i></span></a></td>
                                            <!-- <td>venturi75@free.fr</td> -->
                                        </tr>
                                        <tr>
                                            <td>ESQUIROL</td>
                                            <td>Claude</td>
                                            <td>Membre</td>
                                            <td>2022</td>
                                            <td><span><i
                                                class="material-icons material-icons-outlined md-18">phone_locked</i></span></a></td>
                                            <!-- <td>claude.esquirol@gmail.com</td> -->
                                        </tr>
                                        <tr>
                                            <td>LACOSTE</td>
                                            <td>Bernard</td>
                                            <td>Membre</td>
                                            <td>2021</td>
                                            <td><span><i
                                                class="material-icons material-icons-outlined md-18">phone_locked</i></span></a></td>
                                            <!-- <td>blacoste379@gmail.com</td> -->
                                        </tr>
                                        <tr>
                                            <td>LAFFONT</td>
                                            <td>Vincent</td>
                                            <td>Instructeur</td>
                                            <td>2023</td>
                                            <td><span><i
                                                class="material-icons material-icons-outlined md-18">phone_locked</i></span></a></td>
                                            <!-- <td>vincent.laffont@sncf.fr</td> -->
                                        </tr>
                                        <tr>
                                            <td>LAVIE</td>
                                            <td>Yannick</td>
                                            <td>Membre</td>
                                            <td>2021</td>
                                            <td><span><i
                                                class="material-icons material-icons-outlined md-18">phone_locked</i></span></a></td>
                                            <!-- <td>lavie.yannick@orange.fr</td> -->
                                        </tr>
                                        <tr>
                                            <td>LEGER</td>
                                            <td>Claude</td>
                                            <td>Membre</td>
                                            <td>2021</td>
                                            <td><span><i
                                                class="material-icons material-icons-outlined md-18">phone_locked</i></span></a></td>
                                            <!-- <td>claudeleger07@gmail.com</td> -->
                                        </tr>
                                        <tr>
                                            <td>MARBACH</td>
                                            <td>Bertrand</td>
                                            <td>Instructeur</td>
                                            <td>2022</td>
                                            <td>+33.(0)6.519.777.46</td>
                                            <!-- <td>bertrand.marbach@oumpahpah.fr</td> -->
                                        </tr>
                                        <tr>
                                            <td>MEISTER</td>
                                            <td>Julien</td>
                                            <td>Président</td>
                                            <td>2021</td>
                                            <td><span><i
                                                class="material-icons material-icons-outlined md-18">phone_locked</i></span></a></td>
                                            <!-- <td>meister.julien@free.fr</td> -->
                                        </tr>
                                        <tr>
                                            <td>PRAT</td>
                                            <td>Luc</td>
                                            <td>Instructeur</td>
                                            <td>2022</td>
                                            <td><span><i
                                                class="material-icons material-icons-outlined md-18">phone_locked</i></span></a></td>
                                            <!-- <td>lf.prat@gmail.com</td> -->
                                        </tr>
                                        <tr>
                                            <td>THIBON</td>
                                            <td>Christine</td>
                                            <td>Secrétaire co-optée</td>
                                            <td>-</td>
                                            <td><span><i
                                                class="material-icons material-icons-outlined md-18">phone_locked</i></span></a></td>
                                            <!-- <td>christine@netcys.com</td> -->
                                        </tr>
                                        <tr>
                                            <td>THIBON</td>
                                            <td>Marc</td>
                                            <td>Mécanicien</td>
                                            <td>2022</td>
                                            <td><span><i
                                                class="material-icons material-icons-outlined md-18">phone_locked</i></span></a></td>
                                            <!-- <td>thibonm@laposte.net</td> -->
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>