            <div class="section">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="section-heading heading-center section-heading-animate">
                                <h1>Informations adhérents</h1>
                            </div>
                            <div class="content">
                                <p>Dernière mise à jour de cette page : 16 mars 2024</p>
                                <p>l'Aéroclub de l'Ardèche est une association à but non lucratif régie par les
                                    dispositions de la loi de 1901.</p>
                                <p> <a
                                    <?php if ($grinchLevel != 1013) { ?>
                                    href="<?php echo $extStoragePath; ?>aca-pdf/2403ACA-DTO-Statuts.pdf"
                                    target="_blank">
                                    <?php } ?>
                                    <span><i
                                            class="material-icons material-icons-outlined md-48">download_for_offline</i></span></a>Vous trouverez ici un exemplaire des statuts. Dernière mise à jour du 10/03/2013</p>
                                <p> <a 
                                    <?php if ($grinchLevel != 1013) {?>
                                    href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-Règlement intérieur20191109.pdf"
                                    target="_blank">
                                    <?php } ?>
                                    <span><i
                                            class="material-icons material-icons-outlined md-48">download_for_offline</i></span></a>Vous trouverez ici un exemplaire du règlement intérieur. Dernière mise à jour du
                                    09/11/2019</p>
                                <p>L'aéroclub de l'Ardèche est affilié à la Fédération Française d'Aéronautique</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>