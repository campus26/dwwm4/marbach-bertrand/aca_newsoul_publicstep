                                            <li class="accordion-item section-bgc">
												<div class="accordion-trigger">
													<div><a href="#!" target="_blank" title=""
															style="display: inline-block;">
															<img src="../assets/img/ACA3403-Icone-itineraire-de-vol-2.png"
																alt="icone d'itinéraire de voyage" width="44"
																height="44">
														</a>∾ Log de Nav (Journal de navigation ou routier)
													</div>
												</div>
												<div class="accordion-content content">
													<p>
														<a
															<?php if ($grinchLevel <= 1400) { ?>
															href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-Log_de_Nav_210816_blanc.pdf"
															target="_blank">
															<?php }?>
															<span><i
																	class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Log
														de navigation
														<br>
														<a href="../assets/media/pdf/ACA2403-DTO-NCO-OP125-Fuel-and-oil-supply-ANPI-2016.pdf"
															target="_blank">
															<span><i
																	class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Emport carburant
														<br>
														<a 
															<?php if ($grinchLevel <= 1400) { ?>
															href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-PrepaLogNav-v1.0-240320.pdf"
															target="_blank">
															<?php }?>
															<span><i
																	class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Pour
														bien préparer sa navigation
														<br>
														<a 
															<?php if ($grinchLevel <= 1400) { ?>
															href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-FlashCard-Nav-v1_0_240320.pdf"
															target="_blank">
															<?php }?>
															<span><i
																	class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Flash-Card
														TdP/Local/Navigation
													</p>
												</div>
											</li>