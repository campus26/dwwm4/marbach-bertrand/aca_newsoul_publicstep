                                            <li class="accordion-item section-bgc">
												<div class="accordion-trigger">
													<div><a href="#!" target="_blank" title=""
															style="display: inline-block;">
															<img src="../assets/img/ACA3403-Icone-itineraire.png"
																alt="icone d'itinéraire de voyage" width="44"
																height="44">
														</a>∾ Ressources 'Maison'…
													</div>
												</div>
												<div class="accordion-content content">
													<p>Parfois chapardées ailleurs avec fierté, et montrées ici !
														<br>
														Mais on est partageurs :
														n'hésitez pas à télécharger à votre tour notre production
														locale...
														<br>
														<a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-Refresh-2023-1_3-Tours-de-piste-integration.pdf"
															target="_blank">
															<span><i
																	class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Refresh
														2023
														1/3 Tour de piste, intégration
														<br>
														<a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-Refresh-2023-2_3-Meteo-Perfos-Zp-Manuel-de-Vol.pdf"
															target="_blank">
															<span><i
																	class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Refresh
														2023
														2/3 Météo, Perfos, Zp, Manuel de Vol
														<br><a
															href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-Refresh-2023-2_3-guide-MeteoFrance.pdf"
															target="_blank">
															<span><i
																	class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Refresh
														2023
														2/3 Guide aviation Météo France (2021)
														<br><a
															href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-Refresh-2023-3_3-Principes-de-Navigation-EAC-Communication-Radio.pdf"
															target="_blank">
															<span><i
																	class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Refresh
														2023
														3/3 Principes de Navigation, EAC, Communication Radio
														<br><a
															href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-Refresh-2023-3_3-PHRASEOLOGIE-20230415-(ENAC).pdf"
															target="_blank">
															<span><i
																	class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Refresh
														2023
														3/3 Phraséologie (ENAC) 15/04/2023
													</p>
												</div>
											</li>