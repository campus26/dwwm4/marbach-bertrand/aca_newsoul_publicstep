                                            <li class="accordion-item section-bgc">
												<div class="accordion-trigger">
													<div><a href="#!" target="_blank" title=""
															style="display: inline-block;">
															<img src="../assets/img/ACA2403-Icone-5619685.png"
																alt="icone d'itinéraire de voyage" width="44"
																height="44">
														</a>∾ Reporter un incident (CRESAG)
													</div>
												</div>
												<div class="accordion-content content">
													<p>Remplissez le formulaire et adressez-le au responsable sécurité
														de l'aéroclub (correspondant SGS) ou au responsable
														pédagogique.<br>
														<br> Nous respectons bien entendu l'esprit de la 'Culture Juste' tout comme la confidentialité -et demandons aux adhérents d'en faire de même-
														lorsque nous recueillons un CRESAG. 
														<br><br>
														<a href="../assets/media/pdf/ACA2403-DTO-SGS-CRESAG_TaxVer.5.1.1.2_(C59).pdf"
															target="_blank">
															<span><i
																	class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Formulaire
														de recueil confidentiel d'événement
														<br>
														<a href="../assets/media/pdf/ACA2403-DTO-SGS-guide_culture_juste.pdf"
															target="_blank">
															<span><i
																	class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Guide de
														la culture juste
														<br>
														<a href="../assets/media/pdf/ACA2403-DTO-SGS-GUIDE_Classification_des_risques.pdf"
															target="_blank">
															<span><i
																	class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Guide de
														classification des risques
													</p>
												</div>
											</li>