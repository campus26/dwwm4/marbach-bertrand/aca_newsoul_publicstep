											<li class="accordion-item section-bgc">
												<div class="accordion-trigger">
													<div><a href="#!" target="_blank" title=""
															style="display: inline-block;">
															<img src="../assets/img/ACA3403-Icone-itineraire-de-voyage-2.png"
																alt="icone d'itinéraire de voyage" width="44"
																height="44">
														</a>∾ Dossier de vol
													</div>
												</div>
												<?php if ($grinchLevel <= 1400) { ?>
												<div class="accordion-content content">
													<p><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FBFES-DossierVol.pdf"
															target="_blank">
															<span><i
																	class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Dossier
														de vol F-BFES <br>
														<a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FBPCH-DossierVol.pdf"
															target="_blank">
															<span><i
																	class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Dossier
														de vol F-BPCH <br>
														<a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FBTUL-DossierVol.pdf"
															target="_blank">
															<span><i
																	class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Dossier
														de vol F-BTUL <br>
														<a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FGEBV-DossierVol.pdf"
															target="_blank">
															<span><i
																	class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Dossier
														de vol F-GEBV <br>
														<a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FGKEF-DossierVol.pdf"
															target="_blank">
															<span><i
																	class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Dossier
														de vol F-GKEF <br>
													</p>
												</div>
												<?php } ?>
											</li>