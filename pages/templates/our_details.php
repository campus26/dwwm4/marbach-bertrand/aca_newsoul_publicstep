<div class="contact-info section-bgc">
                                <h3>Ici nos détails</h3>
                                <ul class="contact-list">
                                    <li>
                                        <i class="material-icons material-icons-outlined md-22">location_on</i>
                                        <div class="footer-contact-info">
                                            <a href="https://goo.gl/maps/2Ygp5S2Ssm1G5o329">
                                                560 chemin du flojas, 07120 Labeaume, France
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <i class="material-icons material-icons-outlined md-22">smartphone</i>
                                        <div class="footer-contact-info">
                                            <a href="tel:+33475396257" class="formingHrefTel">+33 4 75 39 62 57 ➞ [fixe]</a>
                                            <a href="tel:+33680847297" class="formingHrefTel">+33 6 80 84 72 97 ➞ [mobile]</a>
                                        </div>
                                    </li>
                                    <li>
                                        <i class="material-icons material-icons-outlined md-22">email</i>
                                        <div class="footer-contact-info">
                                            <a href="mailto:contact@aeroclubdelardeche.com">contact@aeroclubdelardeche.com</a>
                                        </div>
                                    </li>
                                    <li>
                                        <i class="material-icons material-icons-outlined md-22">schedule</i>
                                        <div class="footer-contact-info">
                                            <p><strong>juillet, août</strong> : tous les jours de 9:00 à la nuit
                                                aéronautique</p>
                                            <p><strong>hors-saison</strong> : appelez-nous !</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>